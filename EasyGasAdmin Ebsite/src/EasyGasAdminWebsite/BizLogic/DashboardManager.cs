﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Data;
using EasyGasAdminWebsite.Models.ManageViewModels;

namespace EasyGasAdminWebsite.BizLogic.DashboardManager
{
    public class DashboardManager
    {
        private ApplicationDbContext db;
        public bool DisplayTopMenu = true;
        public bool DisplayDashboardAlert = true;
        private int TopMenuItemsCount = 5;

        DashboardItem dashboardItemModel = new DashboardItem();
        Models.DashboardViewModels.IndexViewModel _dashboardModel;

        public IList<DashboardItem> GetViewItems(string boxType, Models.DashboardViewModels.IndexViewModel dashboardModel)
        {
            try
            {
                List<DashboardItem> dashboardItemModelList = new List<DashboardItem>();
                _dashboardModel = dashboardModel;
                if (boxType == "type1")
                {

                    DashboardItem item1 = GetDelayedOrders();
                    if (item1 != null)
                    {
                        dashboardItemModelList.Add(item1);
                    }
                    DashboardItem item2 = GetOrders();
                    if (item2 != null)
                    {
                        dashboardItemModelList.Add(item2);
                    }
                    DashboardItem item3 = GetTotalRevenue();
                    if (item3 != null)
                    {
                        dashboardItemModelList.Add(item3);
                    }
                    DashboardItem item4 = GetDeliverySlotOrders();
                    if (item4 != null)
                    {
                        dashboardItemModelList.Add(item4);
                    }
                    return dashboardItemModelList;
                }
                else if (boxType == "type2")
                {
                    DashboardItem itemType1 = GetCylindersReturned();
                    if (itemType1 != null)
                    {
                        dashboardItemModelList.Add(itemType1);
                    }
                    
                    DashboardItem itemType3 = GetCancelledOrders();
                    if (itemType3 != null)
                    {
                        dashboardItemModelList.Add(itemType3);
                    }
                    DashboardItem itemType4 = GetIssuesStatus();
                    if (itemType4 != null)
                    {
                        dashboardItemModelList.Add(itemType4);
                    }
                    DashboardItem itemType2 = GetNewCustomers();
                    if (itemType2 != null)
                    {
                        dashboardItemModelList.Add(itemType2);
                    }
                }
                return dashboardItemModelList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected DashboardItem GetCancelledOrders()
        {
            DashboardItem dashboardItem = new DashboardItem();
            float revenue = _dashboardModel.CancelledOrdersCount;
            dashboardItem.count = revenue.ToString();
            dashboardItem.url = "";
            dashboardItem.title = "Cancelled Orders";
            dashboardItem.icon = "fa-list";
            dashboardItem.boxClass = "bg-red";
            return dashboardItem;
        }

        protected DashboardItem GetDeliverySlotOrders()
        {
            var title = "";
            var orders = "";
            foreach (var deliverySlot in _dashboardModel.DeliverySlotList)
            {
                title += deliverySlot.Name + " | ";
                orders += deliverySlot.OrdersToday.ToString() + " | ";
            }
            DashboardItem dashboardItem = new DashboardItem();
            //float revenue = _dashboardModel.CancelledOrdersCount;
            dashboardItem.count = orders;
            dashboardItem.url = "";
            dashboardItem.title = title;
            dashboardItem.icon = "fa-list";
            dashboardItem.boxClass = "bg-aqua";
            return dashboardItem;
        }

        protected DashboardItem GetTotalRevenue()
        {
            DashboardItem dashboardItem = new DashboardItem();
            float revenue = _dashboardModel.AmountCollected;
            dashboardItem.count = revenue.ToString();
            dashboardItem.url = "";
            dashboardItem.title = "Total Revenue";
            dashboardItem.icon = "fa-money";
            dashboardItem.boxClass = "bg-green";
            return dashboardItem;
        }

        protected DashboardItem GetOrders()
        {
            DashboardItem dashboardItem = new DashboardItem();
            int totCount = _dashboardModel.OrdersCount;
            int delCount = _dashboardModel.DeliveredOrdersCount;
            dashboardItem.count = delCount + " / " + totCount;
            dashboardItem.url = "";
            dashboardItem.title = "Delivered / Total Orders";
            dashboardItem.icon = "fa-list";
            dashboardItem.boxClass = "bg-blue";
            return dashboardItem;
        }

        protected DashboardItem GetNewCustomers()
        {
                DashboardItem dashboardItem = new DashboardItem();
                var count = _dashboardModel.NewCustomersCount;
                dashboardItem.count = count.ToString();
                dashboardItem.url = "";
                dashboardItem.title = "New Customers";
                dashboardItem.icon = "fa-user";
                dashboardItem.boxClass = "bg-blue";
                return dashboardItem;
        }

        protected DashboardItem GetDelayedOrders()
        {
            DashboardItem dashboardItem = new DashboardItem();
            var count = _dashboardModel.DelayedOrdersCount;
            dashboardItem.count = count.ToString();
            dashboardItem.url = "";
            dashboardItem.title = "Delayed Orders";
            dashboardItem.icon = "fa-clock-o";
            dashboardItem.boxClass = "bg-orange";
            return dashboardItem;
        }

        protected DashboardItem GetTrucksAvailableForLoading()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "12"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Trucks Available for Loading";
                dashboardItem.icon = "/src/images/vehicles.png";
                dashboardItem.boxClass = "bg-aqua";
                //dashboardItemModelList.Add(dashboardItem);
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetTrucksExpectedForLoading()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "24"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Trucks Expected for Loading";
                dashboardItem.icon = "/src/images/kilometer.png";
                dashboardItem.boxClass = "bg-green";
                //dashboardItemModelList.Add(dashboardItem);
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetCustomersWithLowStockLevels()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "13"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Customers with Low Stock Levels";
                dashboardItem.icon = "fa-battery-0";
                dashboardItem.boxClass = "bg-yellow";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetIncompleteTrips()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "12"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Incomplete Trips";
                dashboardItem.icon = "/src/images/delivery_completed.png";
                dashboardItem.boxClass = "bg-red";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetCustomersWithSalesSurge()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "44"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Customers with sales surge";
                dashboardItem.icon = "/src/images/vehicles.png";
                dashboardItem.boxClass = "bg-blue";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetCustomersWithLevelUpdateFails()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "25"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Customers with level update fails";
                dashboardItem.icon = "/src/images/vehicles.png";
                dashboardItem.boxClass = "bg-aqua";
                return dashboardItem;
            }
            return null;
        }
        protected DashboardItem GetDelayedDeliveries()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "4 / 35"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Delayed Deliveries / Total Deliveries";
                dashboardItem.icon = "fa-clock-o";
                dashboardItem.boxClass = "bg-orange";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetRunningVehicles()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "29 / 47"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Running Vehicles / Used Vehicles";
                dashboardItem.icon = "fa-truck";
                dashboardItem.boxClass = "bg-aqua";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetExternalSystems()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "5 / 32"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "External Systems / Trouble Rectified";
                dashboardItem.icon = "fa-external-link-square";
                dashboardItem.boxClass = "bg-blue";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetIssuesStatus()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "0 / 0"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Urgent Issues / Completed Issues";
                dashboardItem.icon = "fa-check-square";
                dashboardItem.boxClass = "bg-aqua";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetTotalCost()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "133045"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Total Cost for Today";
                dashboardItem.icon = "fa-money";
                dashboardItem.boxClass = "bg-blue";
                return dashboardItem;
            }
            return null;
        }

        protected DashboardItem GetAnomalies()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "3 / 45"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Anomalies / Deviations";
                dashboardItem.icon = "fa-line-chart";
                dashboardItem.boxClass = "bg-green";
                return dashboardItem;
            }
            return null;
        }
        protected DashboardItem GetCylindersReturned()
        {
            bool hasRole = true; // check if access role is there for the user
            if (hasRole)
            {
                DashboardItem dashboardItem = new DashboardItem();
                var count = "0"; //query db
                dashboardItem.count = count;
                dashboardItem.url = "";
                dashboardItem.title = "Cylinders Returned";
                dashboardItem.icon = "fa-arrow-down";
                dashboardItem.boxClass = "bg-yellow";
                return dashboardItem;
            }
            return null;
        }
    }
}
