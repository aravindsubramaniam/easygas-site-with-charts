﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class VehicleLocationsViewModel
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public string UserId { get; set; }
        public Source Source { get; set; }
        public int TenantId { get; set; }
        public int VehicleId { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedAtTimestamp { get; set; }
        public DLoginState DriverLoginState { get; set; }
        public int DLSTimeAgoMin { get; set; }
        public DActivityState DriverActivityState { get; set; }
        public int DASTimeAgoMin { get; set; }
        public int LocUpdatedTimeAgoMin { get; set; }
    }

    public enum Source
    {
        DRIVER_APP = 1,
        SENSEL
    }
}
