﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EasyGasAdminWebsite.Models
{
    public class UserAndProfileModel
    {
        public int TenantId { get; set; }
        public int? BranchId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserType Type { get; set; }
        public Gender Gender { get; set; }
        [Display(Name = "Birth Date")]
        public DateTime? BirthDate { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string Email { get; set; }
        [Required]
        [Display(Name = "Mobile (Username)")]
        public string Mobile { get; set; }
        public string Skype { get; set; }
        public CreationType CreationType { get; set; }
        public string UpdatedBy { get; set; }
        public string FullName()
        {
            string name = FirstName;
            if (!String.IsNullOrEmpty(LastName))
            {
                name += " " + LastName;
            }
            return name;
        }
    }

    public enum UserType
    {
        CUSTOMER = 1,
        DRIVER,
        DISTRIBUTOR
    }

    public enum CreationType
    {
        GUEST,
        USER
    }
}
