﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class UserAndOrderModel
    {
        public UserAndProfileModel UserAndProfile { get; set; }
        public AddressModel Address { get; set; }
        public List<ItemModel> ItemList { get; set; }
        public List<ItemModel> CylinderList { get; set; }
        public List<ItemModel> AccessoriesList { get; set; }
        public List<ItemModel> ServicesList { get; set; }
        public int OrderId { get; set; }
        public int? Quantity { get; set; }
        public double Capacity { get; set; }
        [Required]
        public OrderType Type { get; set; }
        [Required]
        [Display(Name = "Delivery Slot")]
        public int DeliverySlotId { get; set; }
        [Required]
        [Display(Name = "Delivery Date")]
        public DateTime DeliveryDate { get; set; }
        public DateTime DeliveryFrom { get; set; }
        public DateTime DeliveryTo { get; set; }
        [Display(Name = "Vehicle")]
        public int? VehicleId { get; set; }

        public string UserSearch { get; set; }

    }

    public enum Gender
    {
        [Display(Name = "Not Specified")]
        NotSpecified,
        Male,
        Female
    }
}
