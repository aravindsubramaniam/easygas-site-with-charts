﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.CustomerViewModels
{
    public class IndexPageModel
    {
        public List<QueryUserWithDetailsModel> CustomerList { get; set; }
    }
}
