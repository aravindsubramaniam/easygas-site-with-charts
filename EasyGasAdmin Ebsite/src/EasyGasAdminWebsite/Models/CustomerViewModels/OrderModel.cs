﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int Quantity { get; set; }
        public double Capacity { get; set; }
        public OrderType Type { get; set; }
        public int DeliverySlotId { get; set; }
        public string DeliverySlotName { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime DeliveryFrom { get; set; }
        public DateTime DeliveryTo { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserMobile { get; set; }
        public AddressModel Address { get; set; }
        public OrderStatus Status { get; set; }
        public string StatusName { get; set; }
        public int? VehicleId { get; set; }
        public string VehicleRegNo { get; set; }
        public DateTime? VehicleAssignedAt { get; set; }
        public string VehicleAssignedBy { get; set; }
        public DateTime? PlannedDeliveryFrom { get; set; }
        public DateTime? PlannedDeliveryTo { get; set; }

        public int? DeliveredByDriverName { get; set; }
        public int? DeliveredByVehicleIRegNo { get; set; }
        public DateTime? DeliveredAt { get; set; }
        public string DeliveryRemarks { get; set; }

        public int? CancelledByDriverName { get; set; }
        public string CancelledByWebUserId { get; set; }
        public int? CancelledByVehicleRegNo { get; set; }
        public DateTime? CancelledAt { get; set; }
        public string CancelledRemarks { get; set; }

        public int? ItemId { get; set; } // added cylinder

    }

    public enum OrderStatus
    {
        [Display(Name = "Order Created")]
        CREATED,
        [Display(Name = "Order Approved")]
        APPROVED,
        [Display(Name = "Vehicle Assigned")]
        VEHICLE_ASSIGNED,
        [Display(Name = "Vehicle Dispatched")]
        DISPATCHED,
        [Display(Name = "Customer Cancelled")]
        CUSTOMER_CANCELLED,
        [Display(Name = "Order Cancelled")]
        DRIVER_CANCELLED,
        [Display(Name = "Order Delivered")]
        DELIVERED
    }

    public enum OrderType
    {
        [Display(Name = "New Connection")]
        NC = 1,
        [Display(Name = "Refill")]
        REFILL = 2
    }
}
