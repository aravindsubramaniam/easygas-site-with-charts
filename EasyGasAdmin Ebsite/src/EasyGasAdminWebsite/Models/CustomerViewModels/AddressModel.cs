﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class AddressModel
    {
        public int UserAddressId { get; set; }
        [Required]
        public string Location { get; set; }
        [Display(Name = "Latitude")]
        [Required]
        public double? Lat { get; set; }
        [Display(Name = "Longitude")]
        [Required]
        public double? Lng { get; set; }
        [Display(Name = "Building No")]
        public string BuildingNo { get; set; }
        [Display(Name = "Street No")]
        public string StreetNo { get; set; }
        public string Landmark { get; set; }
        public string District { get; set; }
        public string State { get; set; }
        public int? PinCode { get; set; }
        [Display(Name = "Alternate Phone")]
        public string PhoneAlternate { get; set; }
        public string GetCompleteAddress()
        {
            List<string> addrList = new List<string>();
            //string addr = String.Empty;
            if (!String.IsNullOrEmpty(Location))
            {
                addrList.Add(Location);
                //addr += BuildingNo;
            }
            if (!String.IsNullOrEmpty(BuildingNo))
            {
                addrList.Add(BuildingNo);
                //addr += BuildingNo;
            }
            if (!String.IsNullOrEmpty(StreetNo))
            {
                addrList.Add(StreetNo);
                //addr += ", " + StreetNo;
            }
            if (!String.IsNullOrEmpty(Landmark))
            {
                addrList.Add(Landmark);
                //addr += ", " + StreetNo;
            }
            if (!String.IsNullOrEmpty(District))
            {
                addrList.Add(District);
                //addr += ", " + District;
            }
            if (!String.IsNullOrEmpty(State))
            {
                addrList.Add(State);
                //addr += ", " + State;
            }
            if (PinCode > 0)
            {
                addrList.Add( "Pin: " + PinCode);
                //addr += ", Pin: " + PinCode;
            }
            if (!String.IsNullOrEmpty(PhoneAlternate))
            {
                addrList.Add("Phone: " + PhoneAlternate);
                //addr += ", Pin: " + PinCode;
            }
            //return addr;
            return String.Join(", ", addrList.ToArray());
        }
    }
}
