﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class QueryUserWithDetailsModel
    {
        public UserAndProfileModel UserAndProfile { get; set; }
        public List<AddressModel> AddressList { get; set; }
        public List<OrderModel> OrderList { get; set; }
        public List<UserItemModel> UserItemList { get; set; }
    }
}
