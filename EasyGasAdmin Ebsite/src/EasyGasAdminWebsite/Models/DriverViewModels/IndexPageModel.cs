﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.DriverViewModels
{
    public class IndexPageModel
    {
        public List<Driver> DriverList { get; set; }
    }
}
