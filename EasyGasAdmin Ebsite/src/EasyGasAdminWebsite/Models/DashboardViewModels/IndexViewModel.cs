﻿using EasyGasAdminWebsite.Models.OrderViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.DashboardViewModels
{
    public class IndexViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int OrdersCount { get; set; }
        public int DeliveredOrdersCount { get; set; }
        public int UnDeliveredOrdersCount { get; set; }
        public int DelayedOrdersCount { get; set; }
        public int CancelledOrdersCount { get; set; }
        public int NewCustomersCount { get; set; }
        public int AvailableVehiclesCount { get; set; }
        public int TotalVehiclesCount { get; set; }
        public float AmountCollected { get; set; }
        public List<OrderModel> OrdersList { get; set; }
        public List<VehicleModel> VehiclesList { get; set; }
        public List<DeliverySlot> DeliverySlotList { get; set; }
    }
}
