﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class VehicleModel
    {
        public int Id { get; set; }
        [Display(Name = "Driver")]
        public int? DriverId { get; set; }
        public string DriverName { get; set; }

        [Required]
        [Display(Name = "Distributor")]
        public int? DistributorId { get; set; }
        public string DistributorName { get; set; }
        [Display(Name = "Reg No")]
        [Required]
        public string RegNo { get; set; }
        [Display(Name = "Type")]
        [Required]
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        public int MaxCylinders { get; set; }
        public double LastLat { get; set; }
        public double LastLng { get; set; }
        [Display(Name = "Destination Lat")]
        [Required]
        public double DestinationLat { get; set; }
        [Display(Name = "Destination Lng")]
        [Required]
        public double DestinationLng { get; set; }
        public VehicleState State { get; set; }
    }

    public class VehicleType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? TenantId { get; set; }
    }

    public enum VehicleState
    {
        ReadyForWork,
        OutFromWork,
        Break
    }
}
