﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class CreatePricingPageModel
    {
        public List<Item> ItemList { get; set; }
        public List<Tenant> TenantList { get; set; }
        public ItemPricing PricingModel { get; set; }
    }
}
