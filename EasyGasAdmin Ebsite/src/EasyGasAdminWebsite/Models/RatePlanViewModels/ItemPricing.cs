﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class ItemPricing
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        //public virtual Tenant Tenant { get; set; }
        [Required]
        [Display(Name = "Item")]
        public int ItemId { get; set; }
        public Item Item { get; set; }
        public string Description { get; set; }
        [Required]
        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        [Required]
        [Range(1, 9999999999)]
        public float Price { get; set; }
        [Range(0, 9999999999)]
        [Display(Name = "International Price")]
        public float InternationalPrice { get; set; }
    }

    public class Item
    {
        public int Id { get; set; }
        public int? TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public string Code { get; set; }
        public string Specification { get; set; }
        public ItemCategory Category { get; set; }
        public ItemType Type { get; set; }
        public ItemStatus Status { get; set; }
    }

    public class ItemModel
    {
        public int ItemId { get; set; }
        public int? OrderItemId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Specification { get; set; }
        public ItemCategory Category { get; set; }
        public ItemType Type { get; set; }
        public OrderType? OrderType { get; set; }
        public SlotType? SlotType { get; set; }
        public ItemPriceDate PriceDate { get; set; }
        public int Quantity { get; set; }
        public float TotalAmount { get; set; }
    }

    public class UserItemModel
    {
        public int ItemId { get; set; }
        public Item Item { get; set; }
        public int Id { get; set; }
        public int? PurchaseOrderId { get; set; }
        public int? SurrenderOrderId { get; set; }
        public int Quantity { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? ReturnDate { get; set; } 
        public bool IsReturnable { get; set; }
        public UserItemStatus Status { get; set; }
    }

    public enum UserItemStatus
    {
        ItemOrdered,
        ItemReceived,
        ItemSurrendered
    }

    public class ItemPriceDate
    {
        public int ItemId { get; set; }
        public float Price { get; set; }
        public DateTime? FromDate { get; set; }
    }

    public enum ItemStatus
    {
        Inactive = 0,
        Active
    }

    public enum ItemType
    {
        Physical = 1,
        Service
    }

    public enum ItemCategory
    {
        Cylinder = 1,
        Accessories
    }
}
