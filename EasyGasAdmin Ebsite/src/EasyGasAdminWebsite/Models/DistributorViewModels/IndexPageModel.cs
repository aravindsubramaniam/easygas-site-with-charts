﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.DistributorViewModels
{
    public class IndexPageModel
    {
        public List<Distributor> DistributorList { get; set; }
    }
}
