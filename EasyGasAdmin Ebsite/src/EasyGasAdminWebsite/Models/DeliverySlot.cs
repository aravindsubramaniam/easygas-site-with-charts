﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class DeliverySlot
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int OrdersToday { get; set; }
        public SlotType Type { get; set; }
        public int MaxThreshold { get; set; }
    }

    public class DeliverySlotWithDate
    {
        public int SlotId { get; set; }
        public DateTime Date { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int OrdersCount { get; set; }
        public int SlotThreshold { get; set; }
        public bool IsActive { get; set; }
        public string DateName { get; set; }
        public string SlotName { get; set; }
        public SlotType SlotType { get; set; }
    }

    public enum SlotType
    {
        Fixed = 1,
        Variable
    }
}
