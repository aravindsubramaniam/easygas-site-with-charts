﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class Tenant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

    }

    public class Distributor
    {
        public UserAndProfileModel UserProfile { get; set; }
        public AddressModel Address { get; set; }
    }
}
