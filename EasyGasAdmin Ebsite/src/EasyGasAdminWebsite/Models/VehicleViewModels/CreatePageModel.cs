﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.VehicleViewModels
{
    public class CreatePageModel
    {
        public List<VehicleType> VehicleTypeList { get; set; }
        public List<Driver> DriverList { get; set; }
        public List<Distributor> DistributorList { get; set; }
        public VehicleModel Vehicle { get; set; }
    }
}
