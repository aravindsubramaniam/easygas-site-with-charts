﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class Driver
    {
        public UserAndProfileModel UserProfile { get; set; }

    }

    public enum DLoginState
    {
        LoggedIN,
        PauseJob,
        Resume,
        LoggedOut,
    }

    public enum DActivityState
    {
        NoJob,
        HasJob,
        DisplayedJob,
        OrderVerification,
        PauseJob,
        BacktoJob,
        TripStarted,
        DirectionInMaps,
        TripDeliverd,
        FeedbackCaptured
    }

    public class DriverOrderSummaryVM
    {
        public int DriverId { get; set; }
        public string DriverName { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
        public string TenantName { get; set; }
        public string BranchName { get; set; }
        public int OrdersDelivered { get; set; }
        public int OrdersMissed { get; set; }
        public long TravelledMeters { get; set; }
        public long ActiveTimeInMin { get; set; }
        public float RewardPoints { get; set; }

        //universally accepted time format 
        public long StartPeriodUAT { get; set; }
        public long EndPeriodUAT { get; set; }

        public float CashCollected { get; set; }
        public float DaysWorked { get; set; }
        public DLoginState Dls { get; set; }
        public DateTime DlsTime { get; set; }
        public int DlsTimeAgo { get; set; }
        public DActivityState Das { get; set; }
        public DateTime DasTime { get; set; }
        public int DasTimeAgo { get; set; }
    }
}
