﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace EasyGasAdminWebsite.Models.ManageViewModels
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }

        public IList<UserLoginInfo> Logins { get; set; }

        public string PhoneNumber { get; set; }

        public bool TwoFactor { get; set; }

        public bool BrowserRemembered { get; set; }
    }

    public class DashboardItem
    {
        public string count { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string icon { get; set; }
        public string boxClass { get; set; }
    }
}
