﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models
{
    public class ApiValidationErrors
    {
        public string[] Err { get; set; }
    }
}
