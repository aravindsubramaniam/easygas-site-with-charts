﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.OrderViewModels
{
    public class IndexPageModel
    {
        public List<OrderModel> OrdersList { get; set; }
        public List<VehicleModel> VehiclesList { get; set; }
    }
}
