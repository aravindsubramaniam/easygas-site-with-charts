﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.OrderViewModels
{
    public class CreatePageModel
    {
        public List<VehicleModel> VehiclesList { get; set; }
        public List<DeliverySlot> DeliverySlotList { get; set; }
        public List<DeliverySlotWithDate> DeliverySlotWihDateList { get; set; }
        public List<ItemModel> CylinderList { get; set; }
        public List<ItemModel> AccessoriesList { get; set; }
        public List<ItemModel> ServicesList { get; set; }
        public QueryUserWithDetailsModel UserWithDetailsModel { get; set; }
        public OrderModel OrderModel { get; set; }
        public ApiValidationErrors ValidationErrors { get; set; }
    }
}
