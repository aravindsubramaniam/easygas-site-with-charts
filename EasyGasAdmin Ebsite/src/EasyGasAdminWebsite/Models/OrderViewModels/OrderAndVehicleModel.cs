﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.OrderViewModels
{
    public class OrderAndVehicleModel
    {
        public int OrderId { get; set; }
        public int VehicleId { get; set; }
        public string AssignedBy { get; set; }
    }
}
