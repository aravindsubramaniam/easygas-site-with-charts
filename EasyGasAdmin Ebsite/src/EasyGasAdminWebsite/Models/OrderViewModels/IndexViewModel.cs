﻿using EasyGasAdminWebsite.Services.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Models.OrderViewModels
{
    public class IndexViewModel
    {
        public PaginationInfo PaginationInfo { get; set; }
        public List<OrderModel> Orders { get; set; }
    }
}
