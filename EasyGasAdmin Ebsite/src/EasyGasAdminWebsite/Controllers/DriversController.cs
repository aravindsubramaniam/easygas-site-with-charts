﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Controllers
{
    [Authorize]
    public class DriversController : BaseController
    {
        private readonly IDriverService _driverServices;
        private readonly IOptions<ApiSettings> _apiSettings;
        public DriversController(IDriverService driverServices, IOptions<ApiSettings> apiSettings, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
            _driverServices = driverServices;
            _apiSettings = apiSettings;
        }
        public async Task<IActionResult> Index()
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            Models.DriverViewModels.IndexPageModel indexModel = new Models.DriverViewModels.IndexPageModel();
            indexModel = await _driverServices.GetIndexPageModel(tenantId, branchId);
            ViewBag.driversList = indexModel.DriverList;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> View(int id)
        {
            var userWithDetails = await _driverServices.GetDriverDetails(id);
            if (userWithDetails.UserAndProfile != null)
            {
                return View(userWithDetails);
            }
            else
            {
                throw new Exception("Page not found");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            UserAndProfileModel customer = new UserAndProfileModel() { };
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Create";
            return View(customer);
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserAndProfileModel customer)
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            customer.TenantId = tenantId;
            customer.BranchId = branchId;
            customer.Type = UserType.DRIVER;
            if (ModelState.IsValid)
            {
                var result = await _driverServices.CreateDriverAsync(customer);
                if (result.IsSuccessStatusCode)
                {
                    return Redirect("Index");
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Create";
            return View(customer);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var userWithDetails = await _driverServices.GetDriverDetails(id);
            UserAndProfileModel customer = userWithDetails.UserAndProfile;
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Edit";
            return View("Create", customer);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserAndProfileModel customer)
        {
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            if (ModelState.IsValid && customer.UserId > 0)
            {
                var result = await _driverServices.UpdateDriverAsync(customer);
                if (result.IsSuccessStatusCode)
                {
                    return Redirect("Index");
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Edit";
            return View( "Create", customer);
        }
    }
}
