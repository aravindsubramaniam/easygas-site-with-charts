using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EasyGasAdminWebsite.Services;
using Microsoft.Extensions.Options;
using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models.OrderViewModels;
using EasyGasAdminWebsite.Models;
using System.Net;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;

namespace EasyGasAdminWebsite.Controllers
{
    [Authorize]
    public class VehiclesController : BaseController
    {
        private readonly IVehicleService _vehicleServices;
        private readonly IOptions<ApiSettings> _apiSettings;
        public VehiclesController(IVehicleService vehicleService, IOptions<ApiSettings> apiSettings, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
            _vehicleServices = vehicleService;
            _apiSettings = apiSettings;
        }

        public async Task<IActionResult> Index(int page)
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            List<VehicleModel> vehicleList = await _vehicleServices.GetListAsync(0, 10000, tenantId, branchId, null);
            ViewBag.vehicleList = vehicleList;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            VehicleModel vehicle= new VehicleModel(){ IsActive = true};
            Models.VehicleViewModels.CreatePageModel createPageModel = await _vehicleServices.GetCreatePageModel(tenantId, branchId);
            //ViewBag.createPageModel = createPageModel;
            ViewBag.vehTypeSelList = new SelectList(createPageModel.VehicleTypeList, "Id", "Name");
            ViewBag.driverSelList = new SelectList(createPageModel.DriverList, "UserProfile.UserId", "UserProfile.FirstName");
            ViewBag.distributorSelList = new SelectList(createPageModel.DistributorList, "UserProfile.UserId", "UserProfile.FirstName");
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            List<SelectListItem> statusList = new List<SelectListItem> {
                new SelectListItem { Text = "Yes", Value = "true"},
                new SelectListItem { Text = "No", Value = "false"}
            };
            ViewBag.statusList = statusList;
            ViewBag.source = "Create";
            return View(vehicle);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VehicleModel vehicle, string submitButton)
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            if (ModelState.IsValid)
            {
                var result = await _vehicleServices.CreateVehicleAsync(vehicle);
                if (result.IsSuccessStatusCode)
                {
                    if (submitButton == "1")
                    {
                        return Redirect("Create");
                    }
                    else if (submitButton == "2")
                    {
                        return Redirect("Index");
                    }
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            Models.VehicleViewModels.CreatePageModel createPageModel = await _vehicleServices.GetCreatePageModel(tenantId, branchId);
            //ViewBag.createPageModel = createPageModel;
            ViewBag.vehTypeSelList = new SelectList(createPageModel.VehicleTypeList, "Id", "Name");
            ViewBag.driverSelList = new SelectList(createPageModel.DriverList, "UserProfile.UserId", "UserProfile.FirstName");
            ViewBag.distributorSelList = new SelectList(createPageModel.DistributorList, "UserProfile.UserId", "UserProfile.FirstName");
            List<SelectListItem> statusList = new List<SelectListItem> {
                new SelectListItem { Text = "Yes", Value = "true"},
                new SelectListItem { Text = "No", Value = "false"}
            };
            ViewBag.statusList = statusList;
            ViewBag.source = "Create";
            return View(vehicle);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            Models.VehicleViewModels.CreatePageModel createPageModel = await _vehicleServices.GetEditPageModel(id, tenantId, branchId);
            if (createPageModel.Vehicle == null)
            {
                throw new Exception("Page not found");
            }
            VehicleModel vehicle = createPageModel.Vehicle;
            ViewBag.createPageModel = createPageModel;
            ViewBag.vehTypeSelList = new SelectList(createPageModel.VehicleTypeList, "Id", "Name");
            ViewBag.driverSelList = new SelectList(createPageModel.DriverList, "UserProfile.UserId", "UserProfile.FirstName");
            ViewBag.distributorSelList = new SelectList(createPageModel.DistributorList, "UserProfile.UserId", "UserProfile.FirstName");
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            List<SelectListItem> statusList = new List<SelectListItem> {
                new SelectListItem { Text = "Yes", Value = "true"},
                new SelectListItem { Text = "No", Value = "false"}
            };
            ViewBag.statusList = statusList;
            ViewBag.source = "Edit";
            return View("Create", vehicle);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VehicleModel vehicle, string submitButton)
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            if (ModelState.IsValid)
            {
                var result = await _vehicleServices.UpdateVehicleAsync(vehicle);
                if (result.IsSuccessStatusCode)
                {
                    if (submitButton == "1")
                    {
                        return Redirect("Create");
                    }
                    else if (submitButton == "2")
                    {
                        return Redirect("Index");
                    }
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            Models.VehicleViewModels.CreatePageModel createPageModel = await _vehicleServices.GetCreatePageModel(tenantId, branchId);
            //ViewBag.createPageModel = createPageModel;
            ViewBag.vehTypeSelList = new SelectList(createPageModel.VehicleTypeList, "Id", "Name");
            ViewBag.driverSelList = new SelectList(createPageModel.DriverList, "UserProfile.UserId", "UserProfile.FirstName");
            ViewBag.distributorSelList = new SelectList(createPageModel.DistributorList, "UserProfile.UserId", "UserProfile.FirstName");
            List<SelectListItem> statusList = new List<SelectListItem> {
                new SelectListItem { Text = "Yes", Value = "true"},
                new SelectListItem { Text = "No", Value = "false"}
            };
            ViewBag.statusList = statusList;
            ViewBag.source = "Edit";
            return View("Create", vehicle);
        }
    }
}