﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Extensions;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace EasyGasAdminWebsite.Controllers
{
    [Authorize]
    public class RatePlanController : BaseController
    {
        private readonly IRatePlanService _rateplanServices;
        private readonly IOptions<ApiSettings> _apiSettings;
        public RatePlanController(IRatePlanService rateplanService, IOptions<ApiSettings> apiSettings, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
            _rateplanServices = rateplanService;
            _apiSettings = apiSettings;
        }

        public async Task<IActionResult> Index(int page)
        {
            var branchId = await GetCurrentUserBranchId();
            var tenantId = await GetCurrentUserTenantId();
            List<ItemPricing> list = new List<ItemPricing>();
            list = await _rateplanServices.GetListAsync(tenantId, branchId, null);
            ViewBag.list = list;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var branchId = await GetCurrentUserBranchId();
            var tenantId = await GetCurrentUserTenantId();
            ItemPricing pricing = new ItemPricing() { FromDate = DateMgr.GetCurrentIndiaTime() };
            CreatePricingPageModel createPageModel = await _rateplanServices.GetCreatePricingPageModel(tenantId, branchId, null);
            //ViewBag.createPageModel = createPageModel;
            ViewBag.itemSelList = new SelectList(new List<Item>() , "Id", "Name");
            if (createPageModel.ItemList != null)
            {
                if (createPageModel.ItemList.Count > 0)
                {
                    List<Item> cylList = createPageModel.ItemList.Where(p => p.Category == ItemCategory.Cylinder && p.Type == ItemType.Physical).ToList();
                    ViewBag.itemSelList = new SelectList(cylList, "Id", "Name");
                }
            }
            
            //ViewBag.tenantSelList = new SelectList(createPageModel.TenantList, "TenantId", "Name");
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.source = "Create";
            ViewBag.responseMsg = responseMsg;
            return View(pricing);
        }

        [HttpPost]
        public async Task<IActionResult> Create(ItemPricing pricing, string submitButton)
        {
            var branchId = await GetCurrentUserBranchId();
            var tenantId = await GetCurrentUserTenantId();
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            if (ModelState.IsValid)
            {
                var result = await _rateplanServices.CreatePricingAsync(pricing);
                if (result.IsSuccessStatusCode)
                {
                    if (submitButton == "1")
                    {
                        return Redirect("Create");
                    }
                    else if (submitButton == "2")
                    {
                        return Redirect("Index");
                    }
                     {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            CreatePricingPageModel createPageModel = await _rateplanServices.GetCreatePricingPageModel(tenantId, branchId, null); 
            //ViewBag.createPageModel = createPageModel;
            ViewBag.source = "Create";
            ViewBag.itemSelList = new SelectList(new List<Item>(), "Id", "Name");
            if (createPageModel.ItemList != null)
            {
                if (createPageModel.ItemList.Count > 0)
                {
                    List<Item> cylList = createPageModel.ItemList.Where(p => p.Category == ItemCategory.Cylinder).ToList();
                    ViewBag.itemSelList = new SelectList(cylList, "Id", "Name");
                }
            }
            return View(pricing);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var branchId = await GetCurrentUserBranchId();
            var tenantId = await GetCurrentUserTenantId();
            ItemPricing pricing = new ItemPricing() { FromDate = DateMgr.GetCurrentIndiaTime() };
            CreatePricingPageModel createPageModel = await _rateplanServices.GetCreatePricingPageModel(tenantId, branchId, id);
            //ViewBag.createPageModel = createPageModel;
            ViewBag.itemSelList = new SelectList(new List<Item>(), "Id", "Name");
            if (createPageModel.ItemList != null)
            {
                if (createPageModel.ItemList.Count > 0)
                {
                    List<Item> cylList = createPageModel.ItemList.Where(p => p.Category == ItemCategory.Cylinder).ToList();
                    ViewBag.itemSelList = new SelectList(cylList, "Id", "Name");
                }
            }
            if (createPageModel.PricingModel != null)
            {
                pricing = createPageModel.PricingModel;
            }
            //ViewBag.tenantSelList = new SelectList(createPageModel.TenantList, "TenantId", "Name");
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.source = "Edit";
            ViewBag.responseMsg = responseMsg;
            return View("Create", pricing);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ItemPricing pricing, string submitButton)
        {
            var branchId = await GetCurrentUserBranchId();
            var tenantId = await GetCurrentUserTenantId();
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };

            if (ModelState.IsValid && pricing.Id > 0)
            {
                var result = await _rateplanServices.CreatePricingAsync(pricing);
                if (result.IsSuccessStatusCode)
                {
                    if (submitButton == "1")
                    {
                        return Redirect("Create");
                    }
                    else if (submitButton == "2")
                    {
                        return Redirect("Index");
                    }
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            CreatePricingPageModel createPageModel = await _rateplanServices.GetCreatePricingPageModel(tenantId, branchId, null);
            //ViewBag.createPageModel = createPageModel;
            ViewBag.itemSelList = new SelectList(new List<Item>(), "Id", "Name");
            if (createPageModel.ItemList != null)
            {
                if (createPageModel.ItemList.Count > 0)
                {
                    List<Item> cylList = createPageModel.ItemList.Where(p => p.Category == ItemCategory.Cylinder).ToList();
                    ViewBag.itemSelList = new SelectList(cylList, "Id", "Name");
                }
            }
            ViewBag.source = "Edit";
            return View("Create", pricing);
        }
    }
}