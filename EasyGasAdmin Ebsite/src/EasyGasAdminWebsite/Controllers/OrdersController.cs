﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Services;
using System.Net;
using Newtonsoft.Json;
using EasyGasAdminWebsite.Services.Pagination;
using EasyGasAdminWebsite.Configuration;
using Microsoft.Extensions.Options;
using EasyGasAdminWebsite.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace EasyGasAdminWebsite.Controllers
{
    [Authorize]
    public class OrdersController : BaseController
    {
        private readonly IUserService _userServices;
        private readonly IOrderService _orderServices;
        private readonly IVehicleService _vehicleServices;
        private readonly IOptions<ApiSettings> _apiSettings;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;

        public OrdersController(IUserService userServices, IOrderService orderServices, IVehicleService vehService, ISmsSender smsSender, IEmailSender emailSender, IOptions<ApiSettings> apiSettings, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
            _userServices = userServices;
            _orderServices = orderServices;
            _vehicleServices = vehService;
            _apiSettings = apiSettings;
            _emailSender = emailSender;
            _smsSender = smsSender;
        }

        //public IActionResult Index(int page)
        //{
        //    var vm = new Models.OrderViewModels.IndexViewModel()
        //    {
        //        PaginationInfo = new PaginationInfo()
        //        {
        //            ActualPage = page,
        //            ItemsPerPage = 20
        //        }
        //    };

        //    return View(vm);
        //}

        public async Task<IActionResult> Index(int page, string fromDate, string toDate)
        {
            int? branchId = await GetCurrentUserBranchId();
            int tenantId = await GetCurrentUserTenantId();
            if (String.IsNullOrEmpty(fromDate))
            {
                fromDate = DateMgr.GetCurrentIndiaTime().AddDays(-1).ToString("dd-MM-yyyy");
            }
            if (String.IsNullOrEmpty(toDate))
            {
                toDate = DateMgr.GetCurrentIndiaTime().ToString("dd-MM-yyyy");
            }
            Models.OrderViewModels.IndexPageModel indexModel = new Models.OrderViewModels.IndexPageModel();
            indexModel = await _orderServices.GetIndexPageModel(tenantId, branchId, fromDate, toDate, 0, 10000);
            ViewBag.fromDate = fromDate;
            ViewBag.toDate = toDate;
            ViewBag.vehListDict = indexModel.VehiclesList.ToDictionary(p => p.Id, p => p);
            ViewBag.indexModel = indexModel;
            ViewBag.ordersList = indexModel.OrdersList;
            ViewBag.googleMapsApiKey = _apiSettings.Value.GoogleMapsApiKey;
            return View();
        }

        public async Task<Dictionary<int, VehicleLocationsViewModel>> GetVehicleLocations()
        {
            int? branchId = await GetCurrentUserBranchId();
            int tenantId = await GetCurrentUserTenantId();
            Task<List<VehicleLocationsViewModel>> vehLocTask = _orderServices.GetVehicleLocations((int)tenantId, branchId);
            Task<List<DriverOrderSummaryVM>> vehStatusTask = _vehicleServices.GetDriverStatusList((int)tenantId, branchId);
            List<VehicleLocationsViewModel> vehList = await vehLocTask;
            List<DriverOrderSummaryVM> driverSummaryList = await vehStatusTask;
            DateTime now = DateMgr.GetCurrentIndiaTime();
            foreach (var veh in vehList)
            {
                veh.LocUpdatedTimeAgoMin = (int)now.Subtract(veh.CreatedAt).TotalMinutes;
                veh.LocUpdatedTimeAgoMin = veh.LocUpdatedTimeAgoMin == 0 ? 1 : veh.LocUpdatedTimeAgoMin;
                if (driverSummaryList.Any(p => p.VehicleId == veh.VehicleId))
                {
                    veh.DriverActivityState = driverSummaryList.Where(p => p.VehicleId == veh.VehicleId).First().Das;
                    veh.DriverLoginState = driverSummaryList.Where(p => p.VehicleId == veh.VehicleId).First().Dls;
                    veh.DLSTimeAgoMin = driverSummaryList.Where(p => p.VehicleId == veh.VehicleId).First().DlsTimeAgo;
                    veh.DASTimeAgoMin = driverSummaryList.Where(p => p.VehicleId == veh.VehicleId).First().DasTimeAgo;
                }
            }
            Dictionary<int, VehicleLocationsViewModel> data = vehList.ToDictionary(p => p.VehicleId, p => p);
            return data;
        }

        public async Task<ApiValidationErrors> AssignVehicle(int orderId, int vehicleId)
        {
            Models.OrderViewModels.OrderAndVehicleModel orderVehicleModel = new Models.OrderViewModels.OrderAndVehicleModel()
            {
                OrderId = orderId,
                VehicleId = vehicleId,
                //AssignedBy = System.Web.HttpContext.Current.User.Identity.GetUserId() TODO
            };
            ApiValidationErrors errors = await _orderServices.AssignVehicle(orderVehicleModel);
            return errors;
        }

        [HttpGet]
        public async Task<ApiValidationErrors> CancelOrder(int id, string remarks)
        {
            ApiValidationErrors errors = await _orderServices.CancelOrder(id, remarks);
            return errors;
        }

        [HttpGet]
        public async Task<ApiValidationErrors> DeliverOrder(int id, string remarks, string date)
        {
            ApiValidationErrors errors = await _orderServices.DeliverOrder(id, remarks, date);
            return errors;
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            int? branchId = await GetCurrentUserBranchId();
            int tenantId = await GetCurrentUserTenantId();
            Models.OrderViewModels.CreatePageModel createPageModel = await _orderServices.GetCreatePageModel(tenantId, branchId, 0);
            ViewBag.userAddressList = createPageModel.UserWithDetailsModel.AddressList;
            ViewBag.vehSelList = new SelectList(createPageModel.VehiclesList, "Id", "RegNo");
            ViewBag.vehListDict = createPageModel.VehiclesList.ToDictionary(p => p.Id, p => p);
            ViewBag.deliverySlotSelList = new SelectList(createPageModel.DeliverySlotList.Where(p => p.IsActive = true), "Id", "Name");
            ViewBag.cylinderList = createPageModel.CylinderList;
            ViewBag.accessoriesList = createPageModel.AccessoriesList;
            ViewBag.deliverySlotList = createPageModel.DeliverySlotList;
            ViewBag.servicesList = createPageModel.ServicesList;
            ViewBag.cylSelList = new SelectList(createPageModel.CylinderList, "ItemId", "Name");
            UserAndOrderModel CustumerOrder = new UserAndOrderModel
            {
                UserAndProfile = new UserAndProfileModel
                {
                    UserId = 0
                },
                Address = new AddressModel
                {
                    UserAddressId = 0,
                    District = "Bengaluru",
                    State = "Karnataka"
                },
                Quantity = 1,
                DeliveryDate = DateMgr.GetCurrentIndiaTime(),
                CylinderList = createPageModel.CylinderList,
                AccessoriesList = createPageModel.AccessoriesList,
                ServicesList = createPageModel.ServicesList
            };

            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.googleMapsApiKey = _apiSettings.Value.GoogleMapsApiKey;
            ViewBag.source = "Create";
            return View(CustumerOrder);
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserAndOrderModel CustomerOrder, string submitButton)
        {
            int? branchId = await GetCurrentUserBranchId();
            int tenantId = await GetCurrentUserTenantId();
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0]};
            if (ModelState.IsValid)
            {
                //for(var i = 0; i < CustomerOrder.CylinderList.Count(); i++)
                //{
                //    if(CustomerOrder.CylinderList[i].ItemId == CustomerOrder.ItemId)
                //    {
                //        CustomerOrder.CylinderList[i].Quantity = 1;
                //    }
                //}
                CustomerOrder.UserAndProfile.TenantId = tenantId;
                CustomerOrder.UserAndProfile.BranchId = branchId;
                CustomerOrder.UserAndProfile.CreationType = CreationType.USER;
                CustomerOrder.UserAndProfile.Type = UserType.CUSTOMER;
                CustomerOrder.UserAndProfile.UserName = CustomerOrder.UserAndProfile.Password = CustomerOrder.UserAndProfile.Mobile;
                List<ItemModel> cylList = CustomerOrder.CylinderList.Where(p => p.Quantity > 0).ToList();
                List<ItemModel> accList = new List<ItemModel>();
                List<ItemModel> servicesList = new List<ItemModel>();
                if (CustomerOrder.AccessoriesList != null)
                {
                    accList = CustomerOrder.AccessoriesList.Where(p => p.Quantity > 0).ToList();
                }
                if (CustomerOrder.ServicesList != null)
                {
                    servicesList = CustomerOrder.ServicesList.Where(p => p.Quantity > 0).ToList();
                }
                CustomerOrder.ItemList = cylList.Concat(servicesList).Concat(accList).ToList();
                CustomerOrder.Quantity = CustomerOrder.CylinderList.Where(p => p.Quantity > 0).Count();
                var result = await _orderServices.CreateOrderAsync(CustomerOrder);
                if (result.IsSuccessStatusCode)
                {
                    _smsSender.SendSmsToCustomerForOrderConfirmation(CustomerOrder.UserAndProfile.Mobile, CustomerOrder.UserAndProfile.FirstName);
                    _orderServices.PlanNow(tenantId, branchId);
                    if(submitButton == "saveAndCreate")
                    {
                        return Redirect("Create");
                    }
                    else if (submitButton == "saveAndExit")
                    {
                        return Redirect("Index");
                    }
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            Models.OrderViewModels.CreatePageModel createPageModel = await _orderServices.GetCreatePageModel(tenantId, branchId, CustomerOrder.UserAndProfile.UserId);
            ViewBag.userAddressList = createPageModel.UserWithDetailsModel.AddressList;
            ViewBag.vehSelList = new SelectList(createPageModel.VehiclesList, "Id", "RegNo");
            ViewBag.vehListDict = createPageModel.VehiclesList.ToDictionary(p => p.Id, p => p);
            ViewBag.deliverySlotSelList = new SelectList(createPageModel.DeliverySlotList.Where(p => p.IsActive = true), "Id", "Name"); 
            ViewBag.responseMsg = responseMsg;
            ViewBag.googleMapsApiKey = _apiSettings.Value.GoogleMapsApiKey;
            ViewBag.source = "Create";
            ViewBag.cylinderList = createPageModel.CylinderList;
            ViewBag.accessoriesList = createPageModel.AccessoriesList;
            ViewBag.cylSelList = new SelectList(createPageModel.CylinderList, "ItemId", "Name");
            ViewBag.servicesList = createPageModel.ServicesList;
            ViewBag.deliverySlotList = createPageModel.DeliverySlotList;
            return View(CustomerOrder);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            int? branchId = await GetCurrentUserBranchId();
            int tenantId = await GetCurrentUserTenantId();
            Models.OrderViewModels.CreatePageModel createPageModel = await _orderServices.GetEditPageModel(tenantId, branchId, id);
            if (createPageModel.OrderModel == null)
            {
                throw new Exception("Page not found");
            }
            ViewBag.userAddressList = createPageModel.UserWithDetailsModel.AddressList;
            ViewBag.vehSelList = new SelectList(createPageModel.VehiclesList, "Id", "RegNo");
            ViewBag.vehListDict = createPageModel.VehiclesList.ToDictionary(p => p.Id, p => p);
            ViewBag.deliverySlotSelList = new SelectList(createPageModel.DeliverySlotList.Where(p => p.IsActive = true), "Id", "Name");
            UserAndOrderModel CustumerOrder = new UserAndOrderModel
            {
                OrderId = createPageModel.OrderModel.Id,
                UserAndProfile = createPageModel.UserWithDetailsModel.UserAndProfile,
                Address = createPageModel.OrderModel.Address,
                Quantity = createPageModel.OrderModel.Quantity,
                Capacity = createPageModel.OrderModel.Capacity,
                DeliveryFrom = createPageModel.OrderModel.DeliveryFrom,
                DeliverySlotId = createPageModel.OrderModel.DeliverySlotId,
                DeliveryTo = createPageModel.OrderModel.DeliveryTo,
                DeliveryDate = createPageModel.OrderModel.DeliveryDate,
                VehicleId = createPageModel.OrderModel.VehicleId,
                Type = createPageModel.OrderModel.Type
            };

            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.googleMapsApiKey = _apiSettings.Value.GoogleMapsApiKey;
            ViewBag.source = "Edit";
            return View("Create", CustumerOrder);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserAndOrderModel CustomerOrder, string submitButton)
        {
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            if (ModelState.IsValid)
            {
                CustomerOrder.UserAndProfile.CreationType = CreationType.USER;
                CustomerOrder.UserAndProfile.Type = UserType.CUSTOMER;
                CustomerOrder.UserAndProfile.UserName = CustomerOrder.UserAndProfile.Password = CustomerOrder.UserAndProfile.Mobile;
                var result = await _orderServices.EditOrderAsync(CustomerOrder);
                if (result.IsSuccessStatusCode)
                {
                    _smsSender.SendSmsToCustomerForOrderConfirmation(CustomerOrder.UserAndProfile.Mobile, CustomerOrder.UserAndProfile.FirstName);
                    _orderServices.PlanNow(CustomerOrder.UserAndProfile.TenantId, CustomerOrder.UserAndProfile.BranchId);
                    if (submitButton == "saveAndCreate")
                    {
                        return Redirect("Create");
                    }
                    else if (submitButton == "saveAndExit")
                    {
                        return Redirect("Index");
                    }
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            Models.OrderViewModels.CreatePageModel createPageModel = await _orderServices.GetCreatePageModel(CustomerOrder.UserAndProfile.TenantId, CustomerOrder.UserAndProfile.BranchId, CustomerOrder.UserAndProfile.UserId);
            ViewBag.userAddressList = createPageModel.UserWithDetailsModel.AddressList;
            ViewBag.vehSelList = new SelectList(createPageModel.VehiclesList, "Id", "RegNo");
            ViewBag.vehListDict = createPageModel.VehiclesList.ToDictionary(p => p.Id, p => p);
            ViewBag.deliverySlotSelList = new SelectList(createPageModel.DeliverySlotList.Where(p => p.IsActive = true), "Id", "Name");
            ViewBag.responseMsg = responseMsg;
            ViewBag.googleMapsApiKey = _apiSettings.Value.GoogleMapsApiKey;
            ViewBag.source = "Edit";
            return View("Create", CustomerOrder);
        }

        public async Task<List<QueryUserWithDetailsModel>> UserAutoCompleteSearch(String term, string type, int? tenantId, int? branchId)
        {
            //UserService serv = new UserService(apiSettings);
            var data = await _userServices.GetDataAsync(term, 0, 20, tenantId, branchId);
            List<QueryUserWithDetailsModel> model = new List<QueryUserWithDetailsModel>();
            model = data;
            return model;
        }
    }
}