﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Controllers
{
    [Authorize]
    public class DistributorsController : BaseController
    {
        private readonly IDistributorService _distributorServices;
        private readonly IOptions<ApiSettings> _apiSettings;
        public DistributorsController(IDistributorService distributorServices, IOptions<ApiSettings> apiSettings, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
            _distributorServices = distributorServices;
            _apiSettings = apiSettings;
        }

        public async Task<IActionResult> Index()
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            Models.DistributorViewModels.IndexPageModel indexModel = new Models.DistributorViewModels.IndexPageModel();
            indexModel = await _distributorServices.GetIndexPageModel(tenantId, branchId);
            ViewBag.distributorList = indexModel.DistributorList;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> View(int id)
        {
            var userWithDetails = await _distributorServices.Details(id);
            if (userWithDetails.UserProfile != null)
            {
                return View(userWithDetails);
            }
            else
            {
                throw new Exception("Page not found");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            Distributor distributor = new Distributor()
            {
                UserProfile = new UserAndProfileModel
                {
                    Gender = Gender.NotSpecified
                },
                Address = new AddressModel
                {
                    UserAddressId = 0
                }
            };
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Create";
            return View(distributor);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Distributor distributor)
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            distributor.UserProfile.TenantId = tenantId;
            distributor.UserProfile.BranchId = branchId;
            distributor.UserProfile.Type = UserType.DISTRIBUTOR;
            if (ModelState.IsValid)
            {
                var result = await _distributorServices.Create(distributor);
                if (result.IsSuccessStatusCode)
                {
                    return Redirect("Index");
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Create";
            return View(distributor);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            Distributor distributor = await _distributorServices.Details(id);
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Edit";
            return View("Create", distributor);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Distributor distributor)
        {
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            if (ModelState.IsValid && distributor.UserProfile.UserId > 0)
            {
                var result = await _distributorServices.Update(distributor);
                if (result.IsSuccessStatusCode)
                {
                    return Redirect("Index");
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Edit";
            return View("Create", distributor);
        }
    }
}
