﻿﻿using System;
using EasyGasAdminWebsite.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EasyGasAdminWebsite.Controllers
{

    [Authorize]
    public class ReportController : BaseController
    {
        public ReportController(UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
        }

       

        

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult DatewiseReport()
        {
            return View();

        }

        public IActionResult TruckwiseReport()
        {
            return View();

        }


        //public async Task<Dictionary<int, VehicleLocationsViewModel>> GetVehicleLocations()
        //{
        //    int? branchId = await GetCurrentUserBranchId();
        //    int tenantId = await GetCurrentUserTenantId();

        //    Task<List<VehicleLocationsViewModel>> vehLocTask = _orderServices.GetVehicleLocations((int)tenantId, branchId);
        //    List<VehicleLocationsViewModel> vehList = await vehLocTask;



        //}
    }

}