﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Services;
using EasyGasAdminWebsite.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;

namespace EasyGasAdminWebsite.Controllers
{
    [Authorize]
    public class CustomersController : BaseController
    {
        private readonly IUserService _userServices;
        private readonly IOptions<ApiSettings> _apiSettings;
        public CustomersController(IUserService userServices, IOptions<ApiSettings> apiSettings, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
            _userServices = userServices;
            _apiSettings = apiSettings;
        }
        public async Task<IActionResult> Index()
        {
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            Models.CustomerViewModels.IndexPageModel indexModel = await _userServices.GetIndexPageModel(tenantId, branchId);
            ViewBag.customersList = indexModel.CustomerList;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> View(int id)
        {
            var userWithDetails = await _userServices.GetCustomerDetails(id, true);
            if (userWithDetails.UserAndProfile != null)
            {
                return View(userWithDetails);
            }
            else
            {
                throw new Exception("Page not found");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            UserAndProfileModel customer = new UserAndProfileModel() { };
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Create";
            return View(customer);
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserAndProfileModel customer)
        {
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            int tenantId = await GetCurrentUserTenantId();
            int? branchId = await GetCurrentUserBranchId();
            customer.TenantId = tenantId;
            customer.BranchId = branchId;
            customer.Type = UserType.CUSTOMER;
            if (ModelState.IsValid)
            {
                var result = await _userServices.CreateCustomerAsync(customer);
                if (result.IsSuccessStatusCode)
                {
                    return Redirect("Index");
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Create";
            return View(customer);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var userWithDetails = await _userServices.GetCustomerDetails(id, false);
            UserAndProfileModel customer = userWithDetails.UserAndProfile;
            ApiValidationErrors responseMsg = new ApiValidationErrors() { };
            responseMsg.Err = new string[0];
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Edit";
            return View("Create", customer);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserAndProfileModel customer)
        {
            ApiValidationErrors responseMsg = new ApiValidationErrors { Err = new string[0] };
            if (ModelState.IsValid && customer.UserId > 0)
            {
                var result = await _userServices.UpdateCustomerAsync(customer);
                if (result.IsSuccessStatusCode)
                {
                    return Redirect("Index");
                }
                else
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await result.Content.ReadAsStringAsync();
                        responseMsg = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (responseMsg.Err == null)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (responseMsg.Err.Length == 0)
                    {
                        responseMsg.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            ViewBag.responseMsg = responseMsg;
            ViewBag.source = "Edit";
            return View("Create", customer);
        }
    }
}
