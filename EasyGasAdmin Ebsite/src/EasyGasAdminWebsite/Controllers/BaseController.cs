﻿using EasyGasAdminWebsite.Data;
using EasyGasAdminWebsite.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace EasyGasAdminWebsite.Controllers
{
    public class BaseController : Controller
    {
        protected UserManager<ApplicationUser> _userManager;
        protected string _currenUserId;
        public BaseController(UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _currenUserId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        public string GetCurrentUserId()
        {

            try
            {
                return _currenUserId;
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError("Not able to get current user " + ex.Message); ;
            }
            return null;
        }

        public async Task<int> GetCurrentUserTenantId()
        {
            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(_currenUserId);
                if (user != null)
                {
                    return (int)user.TenantId;
                }
                else
                {
                    return 0;
                }
                
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError("Not able to get current user " + ex.Message); ;
            }
            return 0;
        }

        public async Task<int?> GetCurrentUserBranchId()
        {

            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(_currenUserId);
                if (user != null)
                {
                    return user.BranchId;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError("Not able to get current user " + ex.Message); ;
            }
            return null;
        }
    }
}
