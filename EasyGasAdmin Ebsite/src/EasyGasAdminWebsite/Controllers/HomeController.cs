﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyGasAdminWebsite.BizLogic.DashboardManager;
using EasyGasAdminWebsite.Models.ManageViewModels;
using Microsoft.AspNetCore.Authorization;
using EasyGasAdminWebsite.Services;
using EasyGasAdminWebsite.Extensions;
using Microsoft.Extensions.Options;
using EasyGasAdminWebsite.Configuration;
using Microsoft.AspNetCore.Identity;
using EasyGasAdminWebsite.Models;
using Microsoft.AspNetCore.Http;

namespace EasyGasAdminWebsite.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly IDashboardService _dashboardServices;
        private readonly IOptions<ApiSettings> _apiSettings;
        public HomeController(IDashboardService dashboardServices, IOptions<ApiSettings> apiSettings, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor) : base(userManager, httpContextAccessor)
        {
            _dashboardServices = dashboardServices;
            _apiSettings = apiSettings;
        }
        public async Task<IActionResult> Index()
        {
            int tenantId = await GetCurrentUserTenantId();
            var branchId = await GetCurrentUserBranchId();
            DateTime date = DateMgr.GetCurrentIndiaTime();
            string queryDate = date.Date.ToString("dd-MM-yyyy"); 
            //string queryDate = "04-07-2019";
            var dashboardModel = await _dashboardServices.GetInfo(queryDate, tenantId, branchId);
            DashboardManager dashboardManager = new DashboardManager();
            IList<DashboardItem> dashboardItemsType1 = dashboardManager.GetViewItems("type1", dashboardModel);
            IList<DashboardItem> dashboardItemsType2 = dashboardManager.GetViewItems("type2", dashboardModel);
            ViewBag.ordersList = dashboardModel.OrdersList;
            ViewBag.dashboardItemsType1 = dashboardItemsType1;
            ViewBag.dashboardItemsType2 = dashboardItemsType2;
            ViewBag.googleMapsApiKey = _apiSettings.Value.GoogleMapsApiKey;
            ViewBag.vehListDict = dashboardModel.VehiclesList.ToDictionary(p => p.Id, p => p);
            return View();
        }
    }
}
