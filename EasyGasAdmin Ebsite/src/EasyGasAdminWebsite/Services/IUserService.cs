﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EasyGasAdminWebsite.Models;

namespace EasyGasAdminWebsite.Services
{
    public interface IUserService
    {
        int TotalItems { get; }
        Task<List<QueryUserWithDetailsModel>> GetDataAsync(string searchTerm, int from, int? size, int? tenantId, int? branchId);
        Task<Models.CustomerViewModels.IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId);
        Task<HttpResponseMessage> CreateCustomerAsync(UserAndProfileModel customer);
        Task<HttpResponseMessage> UpdateCustomerAsync(UserAndProfileModel customer);
        Task<QueryUserWithDetailsModel> GetCustomerDetails(int id, bool includeOrders);
    }
}
