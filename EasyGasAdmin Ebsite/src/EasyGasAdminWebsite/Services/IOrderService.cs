﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyGasAdminWebsite.Models;
using System.Net.Http;

namespace EasyGasAdminWebsite.Services
{
    public interface IOrderService
    {
        int TotalItems { get; }
        Task<HttpResponseMessage> CreateOrderAsync( UserAndOrderModel CustomerOrder);
        Task<HttpResponseMessage> EditOrderAsync(UserAndOrderModel CustomerOrder);
        Task<List<OrderModel>> GetListAsync(int? tenantId, int? branchId, int from, int? size);
        Task<Models.OrderViewModels.IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId, string fromDate, string toDate, int from, int? size);
        Task<Models.OrderViewModels.CreatePageModel> GetCreatePageModel(int? tenantId, int? branchId, int? UserId);
        Task<Models.OrderViewModels.CreatePageModel> GetEditPageModel(int? tenantId, int? branchId, int OrderId);
        Task<List<VehicleLocationsViewModel>> GetVehicleLocations(int tenantId, int? branchId);
        Task<ApiValidationErrors> AssignVehicle(Models.OrderViewModels.OrderAndVehicleModel OrderVehicleModel);
        Task<ApiValidationErrors> CancelOrder(int orderId, string remarks);
        Task<ApiValidationErrors> DeliverOrder(int orderId, string remarks, string date);
        Task<bool> PlanNow(int tenantId, int? branchId);
    }
}
