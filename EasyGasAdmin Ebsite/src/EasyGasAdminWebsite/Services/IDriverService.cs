﻿using EasyGasAdminWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public interface IDriverService
    {
        Task<Models.DriverViewModels.IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId);
        Task<HttpResponseMessage> CreateDriverAsync(UserAndProfileModel driver);
        Task<HttpResponseMessage> UpdateDriverAsync(UserAndProfileModel driver);
        Task<QueryUserWithDetailsModel> GetDriverDetails(int id);
    }
}
