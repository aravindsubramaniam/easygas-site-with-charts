﻿using EasyGasAdminWebsite.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Models.OrderViewModels;
using Newtonsoft.Json;
using System.Net;

namespace EasyGasAdminWebsite.Services
{
    public class OrderService : IOrderService
    {
        private List<OrderModel> _orders;
        private IndexPageModel _indexPageModel;
        private CreatePageModel _createPageModel;
        private HttpClient _apiClient;
        private readonly IOptions<ApiSettings> _apiSettings;
        private int _totalItems;
        private const int _defaultSize = 20;

        public OrderService(IOptions<ApiSettings> apiSettings)
        {
            _apiSettings = apiSettings;
        }

        public int TotalItems
        {
            get { return _totalItems; }
        }

        public async Task<HttpResponseMessage> CreateOrderAsync(UserAndOrderModel CustomerOrder)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.CreateOrderApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, CustomerOrder);
                
                return response;
            }
        }

        public async Task<HttpResponseMessage> EditOrderAsync(UserAndOrderModel CustomerOrder)
        {
            using (_apiClient = new HttpClient())
            {
                OrderModel order = new OrderModel
                {
                    Id = CustomerOrder.OrderId,
                    Quantity = (int)CustomerOrder.Quantity,
                    Capacity = CustomerOrder.Capacity,
                    DeliveryDate = CustomerOrder.DeliveryDate,
                    DeliverySlotId = CustomerOrder.DeliverySlotId,
                    Type = CustomerOrder.Type,
                    VehicleId = CustomerOrder.VehicleId,
                    Address = CustomerOrder.Address
                };
                var uri = string.Format("{0}", _apiSettings.Value.EditOrderApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, order);

                return response;
            }
        }

        public async Task<List<OrderModel>> GetListAsync(int? tenantId, int? branchId, int from, int? size)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?from={1}&size={2}&tenantId={3}&branchId={4}", _apiSettings.Value.OrdersListApiUrl, from, size ?? _defaultSize, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _orders = JsonConvert.DeserializeObject<List<OrderModel>>(responseJson);
                    IEnumerable<string> values;
                    if (response.Headers.TryGetValues("total", out values))
                    {
                        _totalItems = int.Parse(values.FirstOrDefault());
                    }
                }
            }
            return _orders;
        }

        public async Task<IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId, string fromDate, string toDate, int from, int? size)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?fromDate={1}&toDate={2}&from={3}&size={4}&tenantId={5}&branchId={6}", _apiSettings.Value.OrdersIndexPageApiUrl, fromDate, toDate, from, size ?? _defaultSize, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _indexPageModel = JsonConvert.DeserializeObject<IndexPageModel>(responseJson);
                    IEnumerable<string> values;
                    if (response.Headers.TryGetValues("total", out values))
                    {
                        _totalItems = int.Parse(values.FirstOrDefault());
                    }
                }
            }
            return _indexPageModel;
        }

        public async Task<CreatePageModel> GetCreatePageModel(int? tenantId, int? branchId, int? UserId)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}&UserId={3}", _apiSettings.Value.OrdersCreatePageApiUrl, tenantId, branchId, UserId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _createPageModel = JsonConvert.DeserializeObject<CreatePageModel>(responseJson);
                    IEnumerable<string> values;
                    if (response.Headers.TryGetValues("total", out values))
                    {
                        _totalItems = int.Parse(values.FirstOrDefault());
                    }
                }
            }
            return _createPageModel;
        }

        public async Task<CreatePageModel> GetEditPageModel(int? tenantId, int? branchId, int OrderId)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}&OrderId={3}", _apiSettings.Value.OrdersEditPageApiUrl, tenantId, branchId, OrderId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _createPageModel = JsonConvert.DeserializeObject<CreatePageModel>(responseJson);
                    IEnumerable<string> values;
                    if (response.Headers.TryGetValues("total", out values))
                    {
                        _totalItems = int.Parse(values.FirstOrDefault());
                    }
                }
            }
            return _createPageModel;
        }

        public async Task<List<VehicleLocationsViewModel>> GetVehicleLocations(int tenantId, int? branchId)
        {
            List<VehicleLocationsViewModel> vehList = new List<VehicleLocationsViewModel>();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}", _apiSettings.Value.VehiclePositionsApiUrl, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    vehList = JsonConvert.DeserializeObject<List<VehicleLocationsViewModel>>(responseJson);
                }
            }
            return vehList;
        }

        public async Task<ApiValidationErrors> AssignVehicle(OrderAndVehicleModel OrderVehicleModel)
        {
            ApiValidationErrors errors = new ApiValidationErrors();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.AssignVehicleToOrderApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.PutAsJsonAsync(uri, OrderVehicleModel);
                if (response.IsSuccessStatusCode)
                {
                    //var responseJson = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await response.Content.ReadAsStringAsync();
                        errors = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (errors.Err == null)
                    {
                        errors.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if(errors.Err.Length == 0)
                    {
                        errors.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            return errors;
        }

        public async Task<ApiValidationErrors> CancelOrder(int orderId, string remarks)
        {
            ApiValidationErrors errors = new ApiValidationErrors();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?orderId={1}&vehicleId=&orderId=&remarks={2}&date=", _apiSettings.Value.CancelOrderApiUrl, orderId, remarks);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    //var responseJson = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await response.Content.ReadAsStringAsync();
                        errors = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (errors.Err == null)
                    {
                        errors.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (errors.Err.Length == 0)
                    {
                        errors.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            return errors;
        }

        public async Task<ApiValidationErrors> DeliverOrder(int orderId, string remarks, string date)
        {
            ApiValidationErrors errors = new ApiValidationErrors();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?orderId={1}&vehicleId=&orderId=&remarks={2}&date={3}", _apiSettings.Value.DeliverOrderApiUrl, orderId, remarks, date);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    //var responseJson = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var responseJson = await response.Content.ReadAsStringAsync();
                        errors = JsonConvert.DeserializeObject<ApiValidationErrors>(responseJson);
                    }
                    if (errors.Err == null)
                    {
                        errors.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                    else if (errors.Err.Length == 0)
                    {
                        errors.Err = new string[1] { "Some internal error has occured. Please try again." };
                    }
                }
            }
            return errors;
        }

        public async Task<bool> PlanNow(int tenantId, int? branchId)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}", _apiSettings.Value.PlanNowApiUrl, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
