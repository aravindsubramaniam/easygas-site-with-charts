﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        static string smsApiUrl = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?";
        static string smsUserCred = "user=najeebn@smartpundits.in:arakulam4u&senderID=GRNGDY";
        static string[] smsTemplates =  {
            "Thanks for contacting Total Gaz. Your order is confirmed. Our delivery staff will get in touch with you soon."
        };

        public Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }

        public Task SendSmsAsync(string number, string message)
        {
            return Task.FromResult(0);
        }

        public Task SendSmsToCustomerForOrderConfirmation(string customerPhone, string customerName)
        {
             
            string strUrl = smsApiUrl + smsUserCred + "&state=4&receipientno=" + customerPhone + "&msgtxt=";

            string strData = string.Empty;
            string strFinal = string.Empty;
            strData = smsTemplates[0];
            //strFinal = strData.Replace("{customer}", customerName);
            strFinal = strData;
            try
            {
                strUrl += strFinal;
                bool sendSms = SendSMS(strUrl);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError("Not able to send the message because of " + e.Message);
                //return false;
            }
            return Task.FromResult(0);
        }

        public static bool SendSMS(string SMSstring)
        {
            bool bScuccess = true;
            try
            {
                string dataString = null;
                WebRequest request = HttpWebRequest.Create(SMSstring);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                string str = dataString;
                string value = dataString.Substring(7, 1);
                int retV = 100;
                int.TryParse(value, out retV);
                bScuccess = (retV == 0) ? true : false;
                response.Close();
                s.Close();
                readStream.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(ex.Message);
                //throw;
            }
            return bScuccess;
        }
    }
}
