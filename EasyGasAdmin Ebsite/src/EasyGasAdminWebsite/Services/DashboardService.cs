﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models.DashboardViewModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace EasyGasAdminWebsite.Services
{
    public class DashboardService : IDashboardService
    {
        private IndexViewModel _dashboard;
        private HttpClient _apiClient;
        private readonly IOptions<ApiSettings> _apiSettings;

        public DashboardService(IOptions<ApiSettings> apiSettings)
        {
            _apiSettings = apiSettings;
        }

        public async Task<IndexViewModel> GetInfo(string date, int? tenantId, int? branchId)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?date={1}&tenantId={2}&branchId={3}", _apiSettings.Value.PvtWebDashboardApiUrl, date, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _dashboard = JsonConvert.DeserializeObject<IndexViewModel>(responseJson);
                }
            }
            return _dashboard;
        }
    }
}
