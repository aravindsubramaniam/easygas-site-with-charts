﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public class RatePlanService : IRatePlanService
    {
        private List<ItemPricing> _itemPrices;
        private HttpClient _apiClient;
        private readonly IOptions<ApiSettings> _apiSettings;
        private const int _defaultSize = 10000;

        public RatePlanService(IOptions<ApiSettings> apiSettings)
        {
            _apiSettings = apiSettings;
        }
        public async Task<List<ItemPricing>> GetListAsync(int? tenantId, int? branchId, int? itemId)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}&itemId={3}", _apiSettings.Value.PricingListApiUrl, tenantId, branchId, itemId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _itemPrices = JsonConvert.DeserializeObject<List<ItemPricing>>(responseJson);
                }
            }
            return _itemPrices;
        }

        public async Task<CreatePricingPageModel> GetCreatePricingPageModel(int? tenantId, int? branchId, int? pricingId)
        {
            CreatePricingPageModel model = new CreatePricingPageModel();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}&pricingId={3}", _apiSettings.Value.PricingCreatePageApiUrl, tenantId, branchId, pricingId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<CreatePricingPageModel>(responseJson);
                }
            }
            return model;
        }

        public async Task<HttpResponseMessage> CreatePricingAsync(ItemPricing model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.CreatePricingApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, model);

                return response;
            }
        }
    }
}
