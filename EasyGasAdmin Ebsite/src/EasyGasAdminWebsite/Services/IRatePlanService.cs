﻿using EasyGasAdminWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public interface IRatePlanService
    {
        Task<List<ItemPricing>> GetListAsync(int? tenantId, int? branchId, int? itemId);
        Task<CreatePricingPageModel> GetCreatePricingPageModel(int? tenantId, int? branchId, int? pricingId);
        Task<HttpResponseMessage> CreatePricingAsync(ItemPricing model);
    }
}
