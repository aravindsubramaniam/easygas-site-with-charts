﻿using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Models.VehicleViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public interface IVehicleService
    {
        Task<List<VehicleModel>> GetListAsync(int from, int? size, int? tenantId, int? branchId, int? distributorId);
        Task<HttpResponseMessage> CreateVehicleAsync(VehicleModel  vehicle);
        Task<HttpResponseMessage> UpdateVehicleAsync(VehicleModel vehicle);
        Task<CreatePageModel> GetCreatePageModel(int? tenantId, int? branchId);
        Task<CreatePageModel> GetEditPageModel(int vehicleId, int? tenantId, int? branchId);
        Task<List<DriverOrderSummaryVM>> GetDriverStatusList(int tenantId, int? branchId);
    }
}
