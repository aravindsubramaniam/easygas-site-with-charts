﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Models.CustomerViewModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public class UserService : IUserService
    {
        private List<QueryUserWithDetailsModel> _customers;
        private HttpClient _apiClient;
        private readonly IOptions<ApiSettings> _apiSettings;
        private int _totalItems;
        private const int _defaultSize = 20;

        public int TotalItems
        {
            get { return _totalItems; }
        }

        public UserService(IOptions<ApiSettings> apiSettings)
        {
            _apiSettings = apiSettings;
        }

        public async Task<List<QueryUserWithDetailsModel>> GetDataAsync(string searchTerm, int from, int? size, int? tenantId, int? branchId)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?term={1}&from={2}&size={3}&tenantId={4}&branchId={5}", _apiSettings.Value.CustomerListApiUrl, searchTerm, from, size ?? _defaultSize, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _customers = JsonConvert.DeserializeObject<List<QueryUserWithDetailsModel>>(responseJson);
                    IEnumerable<string> values;
                    if (response.Headers.TryGetValues("total", out values))
                    {
                        _totalItems = int.Parse(values.FirstOrDefault());
                    }
                }
            }
            return _customers;
        }

        public async Task<IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId)
        {
            IndexPageModel indexPageModel = new IndexPageModel();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}", _apiSettings.Value.CustomerIndexPageApiUrl, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    indexPageModel = JsonConvert.DeserializeObject<IndexPageModel>(responseJson);
                }
            }
            return indexPageModel;
        }

        public async Task<HttpResponseMessage> CreateCustomerAsync(UserAndProfileModel model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.CreateCustomerApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<HttpResponseMessage> UpdateCustomerAsync(UserAndProfileModel model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.UpdateCustomerApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PutAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<QueryUserWithDetailsModel> GetCustomerDetails(int userId, bool includeOrders)
        {
            QueryUserWithDetailsModel model = new QueryUserWithDetailsModel();
            using (_apiClient = new HttpClient())
            {
                var uri = "";
                if (includeOrders)
                {
                    uri = string.Format("{0}/{1}", _apiSettings.Value.GetCustomerDetailsWithOrdersApiUrl, userId);
                }
                else
                {
                    uri = string.Format("{0}/{1}", _apiSettings.Value.GetCustomerDetailsApiUrl, userId);
                }

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<QueryUserWithDetailsModel>(responseJson);
                }
                return model;
            }
        }
    }
}
