﻿using EasyGasAdminWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public interface IDistributorService
    {
        Task<Models.DistributorViewModels.IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId);
        Task<HttpResponseMessage> Create(Distributor distributor);
        Task<HttpResponseMessage> Update(Distributor distributor);
        Task<Distributor> Details(int id);
    }
}
