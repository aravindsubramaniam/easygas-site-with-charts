﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyGasAdminWebsite.Models.DashboardViewModels;

namespace EasyGasAdminWebsite.Services
{
    public interface IDashboardService
    {
        Task<IndexViewModel> GetInfo(string date, int? tenantId, int? branchId);
    }
}
