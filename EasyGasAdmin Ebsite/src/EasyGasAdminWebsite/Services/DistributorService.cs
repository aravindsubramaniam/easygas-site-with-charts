﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Models.DistributorViewModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public class DistributorService : IDistributorService
    {
        private HttpClient _apiClient;
        private readonly IOptions<ApiSettings> _apiSettings;
        private const int _defaultSize = 20;

        public DistributorService(IOptions<ApiSettings> apiSettings)
        {
            _apiSettings = apiSettings;
        }

        public async Task<IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId)
        {
            IndexPageModel indexPageModel = new IndexPageModel();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}", _apiSettings.Value.DistributorIndexPageApiUrl, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    indexPageModel = JsonConvert.DeserializeObject<IndexPageModel>(responseJson);
                }
            }
            return indexPageModel;
        }

        public async Task<HttpResponseMessage> Create(Distributor model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.CreateDistributorApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<HttpResponseMessage> Update(Distributor model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.UpdateDistributorApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PutAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<Distributor> Details(int userId)
        {
            Distributor model = new Distributor();
            using (_apiClient = new HttpClient())
            {
                var uri = "";
                uri = string.Format("{0}/{1}", _apiSettings.Value.GetDistributorDetailsApiUrl, userId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<Distributor>(responseJson);
                }
                return model;
            }
        }
    }
}
