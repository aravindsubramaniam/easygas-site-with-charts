﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Models.VehicleViewModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public class VehicleService : IVehicleService
    {
        private List<VehicleModel> _vehicles;
        private HttpClient _apiClient;
        private readonly IOptions<ApiSettings> _apiSettings;
        private const int _defaultSize = 10000;

        public VehicleService(IOptions<ApiSettings> apiSettings)
        {
            _apiSettings = apiSettings;
        }
        public async Task<List<VehicleModel>> GetListAsync(int from, int? size, int? tenantId, int? branchId, int? distributorId)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?from={1}&size={2}&tenantId={3}&branchId={4}&distributorId={5}", _apiSettings.Value.VehiclesListApiUrl, from, size ?? _defaultSize, tenantId, branchId, distributorId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    _vehicles = JsonConvert.DeserializeObject<List<VehicleModel>>(responseJson);
                }
            }
            return _vehicles;
        }

        public async Task<CreatePageModel> GetCreatePageModel(int? tenantId, int? branchId)
        {
            CreatePageModel model = new CreatePageModel();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}", _apiSettings.Value.VehicleCreatePageApiUrl, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<CreatePageModel>(responseJson);
                }
            }
            return model;
        }

        public async Task<CreatePageModel> GetEditPageModel(int vehicleId, int? tenantId, int? branchId)
        {
            CreatePageModel model = new CreatePageModel();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?vehicleId={1}&tenantId={2}&branchId={3}", _apiSettings.Value.VehicleEditPageApiUrl, vehicleId, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<CreatePageModel>(responseJson);
                }
            }
            return model;
        }

        public async Task<HttpResponseMessage> CreateVehicleAsync(VehicleModel model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.CreateVehicleApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<HttpResponseMessage> UpdateVehicleAsync(VehicleModel model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.EditVehicleApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<List<DriverOrderSummaryVM>> GetDriverStatusList(int tenantId, int? branchId)
        {
            List<DriverOrderSummaryVM> driverStatusList = new List<DriverOrderSummaryVM>();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}", _apiSettings.Value.GetDriverStatusListApiUrl, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    driverStatusList = JsonConvert.DeserializeObject<List<DriverOrderSummaryVM>>(responseJson);
                }
            }
            return driverStatusList;
        }
    }
}
