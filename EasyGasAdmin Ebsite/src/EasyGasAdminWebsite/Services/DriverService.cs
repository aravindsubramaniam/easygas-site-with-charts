﻿using EasyGasAdminWebsite.Configuration;
using EasyGasAdminWebsite.Models;
using EasyGasAdminWebsite.Models.DriverViewModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Services
{
    public class DriverService : IDriverService
    {
        private List<QueryUserWithDetailsModel> _drivers;
        private HttpClient _apiClient;
        private readonly IOptions<ApiSettings> _apiSettings;
        private const int _defaultSize = 20;

        public DriverService(IOptions<ApiSettings> apiSettings)
        {
            _apiSettings = apiSettings;
        }

        public async Task<IndexPageModel> GetIndexPageModel(int? tenantId, int? branchId)
        {
            IndexPageModel indexPageModel = new IndexPageModel();
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}?tenantId={1}&branchId={2}", _apiSettings.Value.DriverIndexPageApiUrl, tenantId, branchId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    indexPageModel = JsonConvert.DeserializeObject<IndexPageModel>(responseJson);
                }
            }
            return indexPageModel;
        }

        public async Task<HttpResponseMessage> CreateDriverAsync(UserAndProfileModel model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.CreateDriverApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PostAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<HttpResponseMessage> UpdateDriverAsync(UserAndProfileModel model)
        {
            using (_apiClient = new HttpClient())
            {
                var uri = string.Format("{0}", _apiSettings.Value.UpdateDriverApiUrl);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.PutAsJsonAsync(uri, model);

                return response;
            }
        }

        public async Task<QueryUserWithDetailsModel> GetDriverDetails(int userId)
        {
            QueryUserWithDetailsModel model = new QueryUserWithDetailsModel();
            using (_apiClient = new HttpClient())
            {
                var uri = "";
                uri = string.Format("{0}/{1}", _apiSettings.Value.GetDriverDetailsApiUrl, userId);

                _apiClient.DefaultRequestHeaders.Accept.Clear();
                _apiClient.DefaultRequestHeaders.Add("source", "adminwebsite");
                var response = await _apiClient.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var responseJson = await response.Content.ReadAsStringAsync();
                    model = JsonConvert.DeserializeObject<QueryUserWithDetailsModel>(responseJson);
                }
                return model;
            }
        }
    }
}
