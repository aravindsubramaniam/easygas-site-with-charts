﻿using EasyGasAdminWebsite.Models.OrderViewModels;
using EasyGasAdminWebsite.Services;
using EasyGasAdminWebsite.Services.Pagination;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Views.Shared.Components
{
    public class OrderList : ViewComponent
    {
        private readonly IOrderService _svc;

        public OrderList(IOrderService svc)
        {
            _svc = svc;
        }

        public async Task<IViewComponentResult> InvokeAsync(IndexViewModel vm)
        {
            var data = await _svc.GetListAsync(null, null, vm.PaginationInfo.ActualPage, vm.PaginationInfo.ItemsPerPage);

            vm.Orders = data;
            vm.PaginationInfo.TotalItems = _svc.TotalItems;
            decimal d = ((decimal)_svc.TotalItems / vm.PaginationInfo.ItemsPerPage);
            vm.PaginationInfo.TotalPages = d > (int)d ? (int)d + 1 : (int)d;
            vm.PaginationInfo.Next = (vm.PaginationInfo.ActualPage == vm.PaginationInfo.TotalPages - 1) ? "is-disabled" : "";
            vm.PaginationInfo.Previous = (vm.PaginationInfo.ActualPage == 0) ? "is-disabled" : "";
            vm.PaginationInfo.ItemsPerPage = vm.Orders.Count();

            return View(vm);
        }
    }
}
