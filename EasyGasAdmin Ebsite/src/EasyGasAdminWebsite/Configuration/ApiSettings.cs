﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyGasAdminWebsite.Configuration
{
    public class ApiSettings
    {
        public string OrdersIndexPageApiUrl { get; set; }
        public string OrdersCreatePageApiUrl { get; set; }
        public string OrdersEditPageApiUrl { get; set; }
        public string OrdersListApiUrl { get; set;}
        public string CreateOrderApiUrl { get; set; }
        public string EditOrderApiUrl { get; set; }
        
        public string PvtWebDashboardApiUrl { get; set; }
        public string GoogleMapsApiKey { get; set; }

        public string VehiclePositionsApiUrl { get; set; }
        public string AssignVehicleToOrderApiUrl { get; set; }
        public string CancelOrderApiUrl { get; set; }
        public string DeliverOrderApiUrl { get; set; }

        public string VehiclesListApiUrl { get; set; }
        public string CreateVehicleApiUrl { get; set; }
        public string EditVehicleApiUrl { get; set; }
        public string VehicleCreatePageApiUrl { get; set; }
        public string VehicleEditPageApiUrl { get; set; }
        public string VehicleIndexPageApiUrl { get; set; }

        public string PricingListApiUrl { get; set; }
        public string CreatePricingApiUrl { get; set; }
        public string PricingCreatePageApiUrl { get; set; }
        public string PricingIndexPageApiUrl { get; set; }

        public string CustomerListApiUrl { get; set; }
        public string CustomerIndexPageApiUrl { get; set; }
        public string CreateCustomerApiUrl { get; set; }
        public string UpdateCustomerApiUrl { get; set; }
        public string GetCustomerDetailsApiUrl { get; set; }
        public string GetCustomerDetailsWithOrdersApiUrl { get; set; }

        public string DriverListApiUrl { get; set; }
        public string DriverIndexPageApiUrl { get; set; }
        public string CreateDriverApiUrl { get; set; }
        public string UpdateDriverApiUrl { get; set; }
        public string GetDriverDetailsApiUrl { get; set; }
        public string GetDriverStatusListApiUrl { get; set; }

        public string DistributorListApiUrl { get; set; }
        public string DistributorIndexPageApiUrl { get; set; }
        public string CreateDistributorApiUrl { get; set; }
        public string UpdateDistributorApiUrl { get; set; }
        public string GetDistributorDetailsApiUrl { get; set; }

        public string PlanNowApiUrl { get; set; }
    }
}
